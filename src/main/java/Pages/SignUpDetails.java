package Pages;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SignUpDetails {

	WebDriverWait wait ;


	public SignUpDetails(AndroidDriver driver) {

		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait = new WebDriverWait(driver, 10);
	}



	@AndroidFindBy(id = "com.snapchat.android:id/display_name_first_name_field")
	public AndroidElement firstName;

	@AndroidFindBy(id = "com.snapchat.android:id/display_name_last_name_field")
	public AndroidElement lastName;

	@AndroidFindBy(xpath = "//*[@text='Continue']")
	public AndroidElement continueButton;



	public void enterDetails() {

		try {

			firstName.sendKeys("Appium");
			lastName.sendKeys("training");
			clickOnContinue();

		}catch (Exception e) {
			// TODO: handle exception
		}
	}


	public void clickOnContinue() {

		try {
			continueButton.click();

		}catch (Exception e) {
			// TODO: handle exception
		}

	}






}

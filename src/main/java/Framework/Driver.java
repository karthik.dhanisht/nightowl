package Framework;

import java.net.MalformedURLException;
import java.net.URL;

import javax.management.ConstructorParameters;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;

public class Driver {

	public static AndroidDriver<MobileElement> _driver;
	 

/*
 * This method is used to initialize the android driver
 *  @return 
 */
	@Parameters({"deviceName","udid","platformVersion","appActivity","appPackage"})
	@BeforeTest
	public void setUp(String deviceName,String udid,String platformVersion,String appActivity,String appPackage) {

		try {
			//
			
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, udid);
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
			cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
			cap.setCapability("appActivity", appActivity);
			cap.setCapability("appPackage", appPackage);
			_driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"),cap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


		
		
	@AfterTest	
	 public void quitTest() {
		 
		 try {
			 if(_driver!=null) {
				 
				 _driver.quit();
			 }
			 
		 
		 }catch (Exception e) {
			// TODO: handle exception
		}
		 
	 }
		

		
	  
		
		


	}









